
# Gitリポジトリ標準化

* 作業概要  
	1. 既存プロジェクトにて実運用されている、実績あるものを基に標準化ファイルやツールを準備する。

		* README
		* editconfig
		* gitignore
		* github_issue
		* circleci
		* lintrc  
		<br>
	2. 上記を各言語(フレームワーク)毎に用意したプロジェクトフォルダに配置して必要がある場合は  
	カスタマイズする。



 
## 【README】

* 利用目的  
	作業者が対象プロジェクトの環境構築・ローカル実行・テスト・デプロイの一連を行えるようにする。  
	（場合によっては別wikiへのリンク記載）
	
* ファイル名  
	README.md

* 構成  

	|Category|Description|
	|----|--------|
	|ディレクトリ構成|プロジェクトの各機能ディレクトリの説明|
	|環境構築方法|構築で必要なコマンド、アプリ(Docker等)、設定値の説明)|
	|実行方法|Git Clone、実行コマンド、DBマイグレーション等の説明|
	|テスト方法|テストコマンドの説明|
	|デプロイ方法|デプロイコマンド、CI等の説明|
	|参考文献|参考にしたURLの記載|

* 使用例

```
 # ディレクトリ構成
 ├─include         -> vendor protobuf definition
 ├─internal
 ├─config          -> Environment variable
 └─pkg
    └─middleware   -> gRPC interceptor

 # 環境構築方法
  http://xxx.com/aaa.zipを解凍する。
  >npm install

 # 実行方法
  >npm start

 # テスト方法
  >go test ./...

 # デプロイ方法
  >git push -f origin featureブランチ

 # 参考文献
 http://xxx.co.jp/bbb
   ```


## 【editconfig】

* 利用目的  
	記載したコーディングルールを担保してくれる。
	
* ファイル名  
	.editconfig

* 構成  

	|Type|Example|
	|----|--------|
	|文字コード|`charset = utf-8`|
	|インデントスタイル | `indent_style = space`|
	|インデント半角スペース数|`indent_size = 4`|
	|改行コード種類(lf,cr,crlf)|`end_of_line = lf`|
	|1行最大長さ|`max_line_length = 160`|
	|行末空白の削除|`trim_trailing_whitespace = true`|
	|最終行へ空行追加|`insert_final_newline = true`|

* 使用例

```
[*]  
indent_style = space  
indent_size = 4  
insert_final_newline = true  
trim_trailing_whitespace = true  
end_of_line = lf  
charset = utf-8  
	 
[*.py]  
max_line_length = 160  
```

* [editconfig参考](https://qiita.com/matsu_shiro/items/d0903b455c80c0e023ff)


## 【gitignore】

* 利用目的  
	記載されたフォルダ(ディレクトリ)をバージョン管理対象外とする。
	
* ファイル名  
	.gitignore

* 使用例

```
.idea  
back
.env  
``` 

## 【issue】

* 利用目的  
	内容をチームで共有、タスク割当て、進捗状況の追従、ソースPRとの紐づけ。

	### 使用例（改修案件）

	> ## 要件
	>CSV出力機能を追加する。
	>## 背景
	>現状はお客さんのほうで業務CSVを手作業で作成しているが
	>事業拡大により手作業では限界があるためCSV作成を自動化
	>したいとのこと。
	>## 機能詳細
	>業務テーブルからデータ取得してCSV作成したものをS3に配置する。
	>S3は時限付きURLからダウンロードする。
	>## 必須環境準備
	>S3へのアクセス権限
	>## 優先度
	>中
	>## リリース方法
	>カラム追加のためDBマイグレーション有り
	>サービスをメンテ表示にする必要有り
	>上記以外は通常リリース方法
	>## 対象PR
	>[CSV出力機能](http://github.com/project/pull/xxx)   

	### 使用例（不具合案件）

	> ## 現象
	>CSV出力機能にて純利益の計算が間違っている。
	>## 原因
	>通常は問題ないが、後からお客さんが管理画面上で追加した
	>原価分を考慮していなかった為。
	>## 修正方法
	>追加された原価を読み込んでから計算してCSV出力する。
	>S3は時限付きURLからダウンロードする。
	>## 今後の防止策
	>仕様を把握している人に２人以上のレビューを受ける。
	>## 必須環境準備
	>特になし
	>## 優先度
	>高
	>## リリース方法
	>通常リリース方法
	>## 対象PR
	>[CSV出力機能](http://github.com/project/pull/xxx)

## 【circleci】

* 利用目的  
	テスト、ビルド、デプロイの自動化。（gittoo：github）
	
* 共通で利用したい機能  
	
	|Category|key|Description|
	|----|----|-----|
	|実行環境|`executors` | 定義したdocker環境を複数ジョブで再利用できる|
	|デプロイ|`ecr`|最新ビルドイメージの取得・保存できる|
	|テスト|`postman`|e2eテストを自動化できる|
	|リリース|`ecs or kubernetes`|小～中規模：`ecs`　大規模：`kubernetes`|
	|ライブラリ|`orbs`|aws等のサードパーティ利用でコード短縮できる|
	|キャッシュ|`save_cache,restore_cache`|一度DLしたライブラリ等をキャッシュできる|
	|環境変数|`Environment Variables`|ビルドやデプロイで利用するAWSキーやAPIキーを格納|

## 【lintrc】

* 利用目的  
	コードチェック・改善の自動化。

## other

* [マークダウン インストール](https://qiita.com/SUZUKI_Masaya/items/6476dbbcb3e369640c78)


